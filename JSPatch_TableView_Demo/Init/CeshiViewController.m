
//
//  CeshiViewController.m
//  Init
//
//  Created by mc on 2016/12/16.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "CeshiViewController.h"
#import "AFNetworking.h"

@interface CeshiViewController ()

@property(nonatomic,strong)UITableView *tableView ;

@property(nonatomic,strong)NSArray *dataListArray ;

@end

@implementation CeshiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.hidden = YES ;
    
//    [self _initViews] ;
    
}

/*
#pragma mark - 创建试图 -----------------------------------------------------
// 创建试图
- (void)_initViews
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone ;

}

#pragma mark - 请求数据 -----------------------------------------------------
// 数据请求
- (void)_requestDataList
{
    
//     let urlString = HttpUrl + "/project/list/condition"
//     let httpHeader:[String:String] = ["version":"2.0","terminal":"ios"]
    
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager] ;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 5;
    [manager.requestSerializer setValue:@"2.0" forHTTPHeaderField:@"version"] ;
    [manager.requestSerializer setValue:@"ios" forHTTPHeaderField:@"terminal"] ;
    
    NSString *urlString = @"http://service.bdzcf.com/zcfservice/project/list/condition" ;
    
    NSDictionary *dic = @{@"homeShow":@(1),
                          @"page":@(0)
                          } ;
    
    [manager POST:urlString parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"成功") ;
        
        [self.tableView reloadData] ;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"失败") ;
    }] ;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    self.dataListArray = [NSArray array] ;
    return self.dataListArray.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"] ;
    if(cell == nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"] ;
        cell.textLabel.text = @"赵世杰" ;
        cell.textLabel.textColor = [UIColor grayColor] ;
    }
    [cell setNeedsLayout] ;
    return cell ;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [UIScreen mainScreen].bounds.size.width/2.0+10 ;
}

*/
@end
