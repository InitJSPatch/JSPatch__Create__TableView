//
//  Common.h
//  Init
//
//  Created by mc on 2017/1/7.
//  Copyright © 2017年 zhaoshijie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Common : NSObject

+ (UIImage *)imageWithColor:(UIColor *)color ;

@end
