/**
 * Created by mc on 2016/12/16.
 */

include('TableViewCell.js')
include('HeaderTableViewCell.js')

require('UIColor')
require('UITableViewCell')
require('HeaderTableViewCell')
require('AFHTTPSessionManager, AFJSONResponseSerializer')

defineClass('FirstViewController:UIViewController <UITableViewDelegate,UITableViewDataSource>',
            [
             'tableView',
             'dataListArray',
             'dataListImaArray',
             
             ],
{
            
            viewDidLoad:function(){
                self.super().viewDidLoad() ;
                
                // 从导航栏下面算起
                self.setAutomaticallyAdjustsScrollViewInsets(YES);
                self.setEdgesForExtendedLayout(0);
                
                
                self.__initDataList() ;
                self.__initViews() ;
                self.__requestDataList() ;
            
            },
            
            // 初始化数据
            __initDataList:function(){
//                self.setDataListArray([]);
//                self.setDataListImaArray([]) ;
            },
            
            // 创建视图
            __initViews: function() {
            
                self.navigationController().navigationBar().setHidden(YES) ;
            
                var tabFrame = {x:0, y:0, width:SCREEN_WIDTH, height:SCREEN_HEIGHT} ;
                var tableView = require('UITableView').alloc().initWithFrame(tabFrame) ;
                tableView.setSeparatorStyle(0);
                
                tableView.setDelegate(self);
                tableView.setDataSource(self);
                self.view().addSubview(tableView);
                
                self.setTableView(tableView);
            },
            
            // 网络请求
            __requestDataList:function(){
            
                // 请求cell中的数据
                self.requestContent() ;
                // 请求头试图的数据
                self.requestHeader() ;
            },
            
            // 请求cell中的数据
            requestContent:function(){
            
                var manager = AFHTTPSessionManager.manager();
                manager.setResponseSerializer(AFJSONResponseSerializer.serializer());
                
                manager.requestSerializer().setTimeoutInterval(5);
                manager.requestSerializer().setValue_forHTTPHeaderField("2.0", "version");
                manager.requestSerializer().setValue_forHTTPHeaderField("ios", "terminal");
                
                var urlString = "http://service.bdzcf.com/zcfservice/project/list/condition" ;
                var parametersDic = {"homeShow":1,"page":0} ;
                
                
                // 防止循环引用，一定要加，不然崩溃
                // 使用第三方不需要导入头文件，js文件中是识别不出来的
                var slf = self;
                manager.POST_parameters_progress_success_failure(urlString,parametersDic,null,block('NSURLSessionDataTask *, id',function(task,responseObject){
    //                    console.log(responseObject) ;
                        var data = responseObject["data"] ;
                        if(data){
                            var list = data["list"] ;
                            if(list){
                                slf.setDataListArray(list) ;
                            }
                        }
                        slf.tableView().reloadData() ;
                    }),
                    block('NSURLSessionDataTask *, NSError *',function(task,error){
                        console.log("失败") ;
                    })
                ) ;
            
            
            },
            
            // 请求头试图的数据
            requestHeader:function(){
                var manager = AFHTTPSessionManager.manager();
                manager.setResponseSerializer(AFJSONResponseSerializer.serializer());
                
                manager.requestSerializer().setTimeoutInterval(5);
                manager.requestSerializer().setValue_forHTTPHeaderField("2.0", "version");
                manager.requestSerializer().setValue_forHTTPHeaderField("ios", "terminal");
                
                var urlString = "http://service.bdzcf.com/zcfservice/bbs/banner/image/get" ;
                
                // 防止循环引用，一定要加，不然崩溃
                // 使用第三方不需要导入头文件，js文件中是识别不出来的
                var slf = self;
                manager.POST_parameters_progress_success_failure(urlString,null,null,block('NSURLSessionDataTask *, id',function(task,responseObject){
//                        console.log(responseObject) ;
                        var data = responseObject["data"] ;
                        slf.setDataListImaArray(data) ;
                    }),
                    block('NSURLSessionDataTask *, NSError *',function(task,error){
                       console.log("失败") ;
                    })
                ) ;

            
            },
            
            
            // 代理方法
            tableView_numberOfRowsInSection: function(tableView, section) {
                if(self.dataListArray()){
                    return self.dataListArray().length+1  ;
                }else {
                    return 0 ;
                }
            },
            
            tableView_cellForRowAtIndexPath: function(tableView, indexPath) {
                if(indexPath.row() == 0)
                {
                    var cell = tableView.dequeueReusableCellWithIdentifier("heightCellID") ;
                    if(!cell){
                        cell = HeaderTableViewCell.alloc().initWithStyle_reuseIdentifier(0,"heightCellID") ;
                    }
                    cell.setList(self.dataListImaArray()) ;
                    cell.setNeedsLayout() ;
                    return cell ;
                }
                else
                {
                    var cell = tableView.dequeueReusableCellWithIdentifier("cellID");
                    if (!cell) {
                        cell = TableViewCell.alloc().initWithStyle_reuseIdentifier(0,"cellID");
                        cell.textLabel().setTextColor(UIColor.grayColor());
                    }
                    var list = self.dataListArray()[indexPath.row()-1] ;
                    cell.setList(list) ;
                    cell.setNeedsLayout() ;
                    return cell;
                }
            },
            
            tableView_heightForRowAtIndexPath: function(tableView, indexPath) {
                if(indexPath.row() == 0)
                {
                    return SCREEN_WIDTH/2.0+25+100 ;
                }
                else
                {
                    return SCREEN_WIDTH*2.0/3.0 + 10;
                }
            }
})
