
//include('Common.h')

require('UIView')
require('UIImageView')
require('UIColor')
require('NSURL')
require('UIFont')
require('NSString')
require('UIScrollView')
require('UIPageControl')
require('UISearchBar')
require('Common')
require('UIImage')
require('UILabel')


defineClass('HeaderTableViewCell:UITableViewCell',
            [
                 // 数据
                 'list',
                 // 控件
                'scrollView',
            ],
{
            initWithStyle_reuseIdentifier:function(style,reuseIdentifier){
                self = self.super().initWithStyle_reuseIdentifier(style,reuseIdentifier) ;
                if(self){
                    self.setSelectionStyle(0) ;
                    self.contentView().setBackgroundColor(UIColor.colorWithRed_green_blue_alpha(.9046, .8999, .9093, 1));
            
                    // 创建试图
            self.__initViews() ;
            
                }
                return self ;
            },
            
            // 创建试图
            __initViews:function(){
            
                // 创建滑动试图
                var scrollV = UIScrollView.alloc().initWithFrame({x:0,y:0,width:SCREEN_WIDTH,height:SCREEN_WIDTH/2.0}) ;
                scrollV.setContentSize({width:SCREEN_WIDTH*6.0,height:SCREEN_WIDTH/2.0}) ;
                scrollV.setBackgroundColor(UIColor.redColor()) ;
                scrollV.setPagingEnabled(YES) ;
                scrollV.setShowsHorizontalScrollIndicator(NO) ;
                self.contentView().addSubview(scrollV) ;
                self.setScrollView(scrollV) ;
            
                // 创建图片试图
                for(var i = 0 ; i<6 ; i++)
                {
                    var imageView = UIImageView.alloc().initWithFrame({x:SCREEN_WIDTH*i,y:0,width:SCREEN_WIDTH,height:SCREEN_WIDTH/2.0}) ;
                    imageView.setBackgroundColor(UIColor.grayColor()) ;
                    imageView.setTag(100+i) ;
                    scrollV.addSubview(imageView) ;
                }
            
                // 创建页码试图
                var pageControl = UIPageControl.alloc().initWithFrame({x:(SCREEN_WIDTH-100)/2.0,y:SCREEN_WIDTH/2.0-50,width:100,height:20}) ;
                pageControl.setNumberOfPages(6) ;
                self.contentView().addSubview(pageControl) ;
                
                
                // 创建搜索框
                var searchBar = UISearchBar.alloc().initWithFrame({x:30,y:SCREEN_WIDTH/2.0-15,width:SCREEN_WIDTH-60,height:30}) ;
                searchBar.setPlaceholder('请输入项目名称') ;
                searchBar.setBackgroundColor(UIColor.clearColor()) ;
                searchBar.setBackgroundImage(Common.imageWithColor(UIColor.clearColor())) ;
                self.contentView().addSubview(searchBar) ;
            
                // 创建button底部UIView
                var bottomView = UIView.alloc().initWithFrame({x:5,y:SCREEN_WIDTH/2.0+25,width:SCREEN_WIDTH-10,height:100}) ;
                bottomView.setBackgroundColor(UIColor.whiteColor()) ;
                self.contentView().addSubview(bottomView) ;
            
            
            var imgArray = ['zc_menu_1','zc_menu_2','zc_menu_3','zc_menu_4'] ;
            var stringArray = ['积分兑换','热销榜单','他们说','众筹攻略'] ;
            // 创建
            for(var i = 0 ; i<4 ; i++)
            {
            
                // 创建imageView按钮，本来是button的，没有点击事件为了省事换成imageView
                var imageView = UIImageView.alloc().initWithFrame({x:0,y:10,width:(SCREEN_WIDTH-10)/4.0-20,height:(SCREEN_WIDTH-10)/4.0-20}) ;
                imageView.setCenter({x:((SCREEN_WIDTH-10)/4.0)*i+(SCREEN_WIDTH-10)/4.0/2.0,y:40}) ;
                imageView.setImage(UIImage.imageNamed(imgArray[i])) ;
                bottomView.addSubview(imageView) ;
            
                // 创建button的文本
                var buttonLabel = UILabel.alloc().initWithFrame({x:0,y:0,width:(SCREEN_WIDTH-10)/4.0-20,height:20}) ;
                buttonLabel.setCenter({x:((SCREEN_WIDTH-10)/4.0)*i+(SCREEN_WIDTH-10)/4.0/2.0,y:85}) ;
                buttonLabel.setFont(UIFont.systemFontOfSize(14)) ;
                buttonLabel.setTextAlignment(1) ;
                buttonLabel.setText(stringArray[i]) ;
                bottomView.addSubview(buttonLabel) ;
            
            }
            
            },
            
            
            
            
            // 刷新方法
            layoutSubviews:function(){
                self.super().layoutSubviews() ;
                
                var list = self.list() ;
                if(list)
                {
                    for(var i = 0 ; i<6 ; i++)
                    {
                        var dataList = list[i] ;
                        console.log(dataList['url']) ;
                        var imageView = self.scrollView().viewWithTag(100+i) ;
                        var url = NSURL.URLWithString(dataList['url']) ;
                        imageView.sd__setImageWithURL(url) ;
                    }
                }
            }
            
})
