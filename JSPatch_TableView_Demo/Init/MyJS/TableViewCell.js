/**
 * Created by mc on 2016/12/16.
 */

require('UIView')
require('UIImageView')
require('UILabel')
require('UIColor')
require('NSURL')
require('UIFont')
require('NSString');

defineClass('TableViewCell:UITableViewCell',
            [
             
             // 数据
             'list',
             
             // 试图
             'bgView',              // 背景试图
             'imgView',             // 背景图片
             'supportNumLabel',     // 支持者的个数
             'roomNameLabel',       // 房屋的名称
             'amountLabel' ,        // 起购金额
             'targetNumLabel',      // 目标份额
             
             ],
{
            
            
            initWithStyle_reuseIdentifier:function(style,reuseIdentifier){
                self = self.super().initWithStyle_reuseIdentifier(style,reuseIdentifier) ;
                if(self){
                    self.setSelectionStyle(0) ;
                    self.contentView().setBackgroundColor(UIColor.colorWithRed_green_blue_alpha(.9046, .8999, .9093, 1));
            
                    self.__initViews() ;
                }
                return self ;
            },
            
            __initViews:function(){
            
                // 背景试图
                var bgView = UIView.alloc().initWithFrame({x:5,y:5,width:SCREEN_WIDTH-10,height:SCREEN_WIDTH*2.0/3.0}) ;
                bgView.setBackgroundColor(UIColor.whiteColor()) ;
                self.contentView().addSubview(bgView) ;
                self.setBgView(bgView) ;
            
                // 图片
                var imageView = UIImageView.alloc().initWithFrame({x:0,y:0,width:SCREEN_WIDTH-10,height:SCREEN_WIDTH*2.0/3.0-40}) ;
                imageView.setBackgroundColor(UIColor.redColor()) ;
                imageView.setContentMode(0);
                self.bgView().addSubview(imageView) ;
                self.setImgView(imageView) ;
            
                // 支持者标题:（支持者：）
                var supportNumTitleLab = UILabel.alloc().initWithFrame({x:10,y:10,width:45,height:15}) ;
                supportNumTitleLab.setTextColor(UIColor.whiteColor()) ;
                supportNumTitleLab.setFont(UIFont.systemFontOfSize(12));
                supportNumTitleLab.setTextAlignment(0) ;
                supportNumTitleLab.setText('支持者:') ;
                self.bgView().addSubview(supportNumTitleLab) ;
            
                // 支持者数据:(100)
                var supportNumLab = UILabel.alloc().initWithFrame({x:10+45,y:10,width:100,height:15}) ;
                supportNumLab.setTextColor(UIColor.whiteColor()) ;
                supportNumLab.setFont(UIFont.systemFontOfSize(12)) ;
                supportNumLab.setTextAlignment(0) ;
                supportNumLab.setText('0') ;
                self.bgView().addSubview(supportNumLab) ;
                self.setSupportNumLabel(supportNumLab) ;
            
            
                // 房屋的名称:(白马山庄)
                var roomNameLab = UILabel.alloc().initWithFrame({x:10,y:SCREEN_WIDTH*2.0/3.0-40-40,width:SCREEN_WIDTH-30,height:40}) ;
                roomNameLab.setTextAlignment(0);
                roomNameLab.setTextColor(UIColor.whiteColor());
                self.bgView().addSubview(roomNameLab);
                self.setRoomNameLabel(roomNameLab);
            
            
                // 起购金额:(20000)
                var buyLab = UILabel.alloc().initWithFrame({x:0,y:0,width:(SCREEN_WIDTH-10)/2.0,height:20}) ;
                buyLab.setCenter({x:(SCREEN_WIDTH-10)/4.0,y:SCREEN_WIDTH*2.0/3.0-40+15}) ;
                buyLab.setTextAlignment(1) ;
                buyLab.setTextColor(UIColor.blackColor()) ;
                buyLab.setText('0') ;
                self.bgView().addSubview(buyLab) ;
                self.setAmountLabel(buyLab) ;
                
                // 起购金额标题:(起购金额)
                var buyTitleLab = UILabel.alloc().initWithFrame({x:0,y:0,width:(SCREEN_WIDTH-10)/2.0,height:20}) ;
                buyTitleLab.setCenter({x:(SCREEN_WIDTH-10)/4.0,y:SCREEN_WIDTH*2.0/3.0-40+15+15}) ;
                buyTitleLab.setFont(UIFont.systemFontOfSize(10)) ;
                buyTitleLab.setTextAlignment(1) ;
                buyTitleLab.setTextColor(UIColor.grayColor()) ;
                buyTitleLab.setText('起购金额') ;
                self.bgView().addSubview(buyTitleLab) ;
            
            // 目标份额:(1000)
            var targetNumLab = UILabel.alloc().initWithFrame({x:0,y:0,width:(SCREEN_WIDTH-10)/2.0,height:20}) ;
            targetNumLab.setCenter({x:(SCREEN_WIDTH-10)/4.0*3,y:SCREEN_WIDTH*2.0/3.0-40+15}) ;
            targetNumLab.setTextAlignment(1) ;
            targetNumLab.setTextColor(UIColor.blackColor()) ;
            targetNumLab.setText('0') ;
            self.bgView().addSubview(targetNumLab) ;
            self.setTargetNumLabel(targetNumLab) ;
            
            // 目标份额标题:(目标份额)
            var targetNumTitleLab = UILabel.alloc().initWithFrame({x:0,y:0,width:(SCREEN_WIDTH-10)/2.0,height:20}) ;
            targetNumTitleLab.setCenter({x:(SCREEN_WIDTH-10)/4.0*3,y:SCREEN_WIDTH*2.0/3.0-40+15+15}) ;
            targetNumTitleLab.setFont(UIFont.systemFontOfSize(10)) ;
            targetNumTitleLab.setTextAlignment(1) ;
            targetNumTitleLab.setTextColor(UIColor.grayColor()) ;
            targetNumTitleLab.setText('目标份额') ;
            self.bgView().addSubview(targetNumTitleLab) ;
            
            },
            
            // 刷新方法
            layoutSubviews:function(){
                self.super().layoutSubviews() ;
            
                var dataList = self.list() ;
                // 使用全局的数据
                if(dataList)
                {
            
                    self.supportNumLabel().setText(NSString.stringWithFormat("%d", dataList['supportNum'])) ;
                    var url = NSURL.URLWithString(dataList['listImageUrl']) ;
                    self.imgView().sd__setImageWithURL(url) ;
                    self.roomNameLabel().setText(dataList['label']) ;
                    self.amountLabel().setText(NSString.stringWithFormat("%d", dataList['amount'])) ;
                    self.targetNumLabel().setText(NSString.stringWithFormat("%d", dataList['targetNum'])) ;
                }
            }
            
})
