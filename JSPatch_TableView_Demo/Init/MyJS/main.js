autoConvertOCType(1)
include('CommonDefine.js')
include('FirstViewController.js')


defineClass('AppDelegate',{
    initRootViewController:function(){

        var firstVC = require('FirstViewController').alloc().init() ;
        var navCrtl = require('UINavigationController').alloc().initWithRootViewController(firstVC) ;
        self.window().setRootViewController(navCrtl) ;
    }
})
