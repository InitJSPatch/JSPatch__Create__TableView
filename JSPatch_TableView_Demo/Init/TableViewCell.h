//
//  TableViewCell.h
//  Init
//
//  Created by mc on 2016/12/16.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell


@property(nonatomic,strong)UIView *bgView ;



@property(nonatomic,strong)UIImageView *imgView ;

@property(nonatomic,strong)UILabel *label ;


@property(nonatomic,strong)NSString *urlImg ;

@property(nonatomic,strong)NSString *titleString ;




@end
