//
//  TableViewCell.m
//  Init
//
//  Created by mc on 2016/12/16.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "TableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Common.h"


@implementation TableViewCell

/*
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] ;
    if(self){
        
        [self _initViews] ;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        self.contentView.backgroundColor = [UIColor colorWithRed:0.9046 green:0.8999 blue:0.9093 alpha:1] ;
    }
    return self ;
}


#pragma mark - 创建试图 -----------------------------------------------------
// 创建试图
- (void)_initViews
{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width-10, ([UIScreen mainScreen].bounds.size.width)/2.0)] ;
    [self.contentView addSubview:bgView] ;
    bgView.backgroundColor = [UIColor whiteColor] ;
    self.bgView = bgView ;
    
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-10, ([UIScreen mainScreen].bounds.size.width)/2.0-40)] ;
    imageView.backgroundColor = [UIColor redColor] ;
    imageView.contentMode = 2;
    imageView.clipsToBounds = YES ;
    [self.contentView addSubview:imageView] ;
    self.imgView = imageView ;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(40, ([UIScreen mainScreen].bounds.size.width)/2.0-40+10, 100, 20)] ;
    label.textAlignment = NSTextAlignmentLeft ;
    label.textColor = [UIColor grayColor] ;
    label.backgroundColor = [UIColor purpleColor] ;
    [self.contentView addSubview:label] ;
    self.label = label ;
    
    label.text = [NSString stringWithFormat:@"%d",1] ;
    
    label.font = [UIFont systemFontOfSize:12] ;
    
    label.center = CGPointMake(100, 100) ;
    
    
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width/3*2.0)] ;
    scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width*5, [UIScreen mainScreen].bounds.size.width/3*2.0) ;
    scrollView.pagingEnabled = YES ;
    scrollView.showsHorizontalScrollIndicator = NO ;
    
    for(int i = 0 ; i < 5 ; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]init] ;
        imageView.backgroundColor = [UIColor grayColor] ;
        imageView.tag = 100+i ;
        imageView.image = [UIImage imageNamed:@""] ;
        
    }
    
    
}


- (void)layoutSubviews
{
    [super layoutSubviews] ;
    NSURL *url = [NSURL URLWithString:self.urlImg] ;
    [self.imageView sd_setImageWithURL:url] ;
    self.label.text = self.titleString ;
    
    UIPageControl *pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 0, 100, 20)] ;
    pageControl.numberOfPages = 2;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.pageIndicatorTintColor = [UIColor blueColor];
    [self.contentView addSubview:pageControl];
    
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 100, 30)] ;
    [searchBar setPlaceholder:@"Search"];
    [self.contentView addSubview:searchBar] ;
    searchBar.backgroundImage = [Common imageWithColor:[UIColor clearColor]] ;
    
    
}

*/


@end
